import type { ResultAddress } from "@/types/address"
import { defineStore } from "pinia"
import { ref } from 'vue'

export const userAddressesStore = defineStore('address', () => {
    // 存储地址
    const addressList = ref<ResultAddress>()

    // 交换地址方法
    const setAddress = (val: ResultAddress) => {
        addressList.value = val
    }

    return {
        addressList,
        setAddress
    }
},
    // 持久化储存
    {
        persist: {
            storage: {
                getItem(key) {
                    return uni.getStorageSync(key)
                },
                setItem(key: any, value) {
                    uni.setStorageSync(key, value)
                },
            },
        },
    }
)