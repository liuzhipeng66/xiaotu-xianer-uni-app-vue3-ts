import { defineStore } from 'pinia'
import type { loginItem } from "@/types/login"
import { ref } from 'vue'

// 定义 Store
export const useMemberStore = defineStore(
  'member',
  () => {
    // 会员信息
    const profile = ref<loginItem>()
    const token = ref<string>()

    // 保存会员信息，登录时使用
    const setProfile = (val: loginItem) => {
      profile.value = val
      token.value = val.token
    }

    // 清理会员信息，退出时使用
    const clearProfile = () => {
      profile.value = undefined
      token.value = ''
    }

    // 记得 return
    return {
      profile,
      token,
      setProfile,
      clearProfile,
    }
  },
  // TODO: 持久化
  {
    persist: {
      storage: {
        getItem(key) {
          return uni.getStorageSync(key)
        },
        setItem(key: any, value) {
          uni.setStorageSync(key, value)
        },
      },
    },
  },
)
