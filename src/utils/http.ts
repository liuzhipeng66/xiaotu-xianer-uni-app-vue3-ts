/**
 *
 * 添加拦截器：
 *   拦截 request 请求
 *   拦截 uploadFile 文件上传
 *
 *
 *  TODO:
 *  1. 非 http 开头需拼接地址(options.url + baseURL)
 *  2. 请求超时（timeout）
 *  3. 添加小程序端请求头标识（header）
 *  4. 添加 token 请求头标识（options.header.Authorization = token）
 *
 */
import { useMemberStore } from '@/stores'

const baseURL = 'https://pcapi-xiaotuxian-front-devtest.itheima.net'

// 添加响应拦截器
const httpInterceptor = {
    invoke(options: UniApp.RequestOptions) {
        // 1. 非 http 开头需拼接地址
        // indexOf: 没有找到返回-1
        if (options.url.indexOf('http') === -1) {
            options.url = baseURL + options.url
        }
        // startsWith： 查找字符串 有返回true 没有返回false
        // if (!options.url.startsWith('http')) {
        //     options.url = baseURL + options.url
        // }
        // 2. 请求超时 默认60s
        options.timeout = 10000
        // 3. 添加小程序端请求头标识
        options.header = {
            // 如果有提交的header就保存
            ...options.header,
            // 小程序标识
            'source-client': 'miniapp',
        }
        // 4. 添加 token 请求头标识
        const memberStore = useMemberStore()
        const token = memberStore?.token
        // 判断是否有token
        if (token) {
            // 添加请求头
            options.header.Authorization = token
        }
    },
}
// 拦截 request 请求
uni.addInterceptor('request', httpInterceptor)
// 拦截 uploadFile 文件上传
uni.addInterceptor('uploadFile', httpInterceptor)


// 获取数据类型（返回）
interface Data<T> {
    code: string
    message: string
    msg: string
    result: T
}
// 请求响应函数封装 ( 使用Promise )
export const http = <T>(options: UniApp.RequestOptions) => {
    return new Promise<Data<T>>((resolve, reject) => {
        // 发请求
        uni.request({
            ...options,
            // 成功回调
            success(res) {
                if (res.statusCode >= 200 && res.statusCode < 300) {
                    resolve(res.data as Data<T>)
                    // 401
                } else if (res.statusCode === 401) {
                    // 清除数据
                    const memberStore = useMemberStore()
                    // 调用方法（清除数据）
                    memberStore.clearProfile()
                    // 数据失效，请重新登录
                    uni.showToast({
                        title: '请重新登录， 亲！',
                        icon: 'none'
                    })
                    // 跳转
                    uni.navigateTo({
                        url: '/pages/login/login'
                    })
                    reject(res)
                    // 请求错误
                } else {
                    uni.showToast({
                        // 断言类型
                        title: (res.data as Data<T>).msg || '请求错误',
                        icon: 'none'
                    })
                    return
                }
            },
            // 失败回调
            fail(err) {
                uni.showToast({
                    title: '当前网络服务差',
                    icon: 'none',
                })
                reject(err)
            }
        })
    })
}
