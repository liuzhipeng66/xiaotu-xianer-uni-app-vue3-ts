// 这里存储购物车数据类型
export type cartItem = {
    attrsText: string
    count: number
    discount: string
    id: string
    isCollect: boolean
    isRffective: boolean
    name: string
    nowOriginalPrice: string
    nowPrice: string
    picture: string
    postFee: number
    price: string
    selected: boolean
    skuId: string
    specs: specsString[]
    stock: number
}
export type specsString = {
    name: string
    valueName: string
}