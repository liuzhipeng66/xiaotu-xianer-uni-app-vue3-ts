// 这里存储首页的数据类型

// 轮播图数据类型
export type BannerItem = {
    hrefUrl: string
    id: string
    imgUrl: string
    type: string
}

// 分类数据类型
export type CategoryItem = {
    icon: string
    id: string
    name: string
}

// 热门推荐数据类型
export type HotItem = {
    alt: string
    id: string
    pictures: string[]
    target: string
    title: string
    type: number
}

