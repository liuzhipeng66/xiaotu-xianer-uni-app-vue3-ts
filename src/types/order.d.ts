// 这里存储订单数据类型
export type orderItem = {
    goods: goodsOrder[]
    summary: summaryOrder
    userAddresses: userAddressesOrder[]
}
// goodsOrder
export type goodsOrder = {
    attrsText: string
    count: number
    id: string
    name: string
    payPrice: string
    picture: string
    price: string
    skuId: string
    totalPayPrice: string
    totalPrice: string
}
// summaryOrder
export type summaryOrder = {
    discountPrice: number
    goodsCount: number
    postFee: number
    totalPayPrice: number
    totalPrice: number
}
// userAddressesOrder
export type userAddressesOrder = {
    address: string
    addressTags: string
    cityCode: string
    contact: string
    countyCode: string
    fullLocation: string
    id: string
    isDefault: number
    postalCode: string
    provinceCode: string
    receiver: string
}
