// 这里存储订单支付类型
import { GoodsItem } from "./global"
import { detailOrder } from "./detail"

// 物流数据类型
export type OrderLogistics = {
    company: companyLogistics
    count: number
    list: listLogistics[]
    picture: string
}
export type companyLogistics = {
    name: string
    number: string
    tel: string
}
export type listLogistics = {
    id: string
    text: string
    time: string
}

// 获取订单列表(参数)
export type getOrderParams = {
    page?: number
    pageSize?: number
    orderState?: number
}
// 获取订单列表(数据)
export type getOrderItem = & GoodsItem<detailOrder>
