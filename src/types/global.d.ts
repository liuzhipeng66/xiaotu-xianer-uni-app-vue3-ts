// 这里是全局类型

import type { subTypesItem } from "./hot"

// 猜你喜欢数据类型
export type GoodsItem<T> = {
    counts: string
    items: T[]
    page: number
    pageSize: number
    pages: number
}

// 页数参数类型
export type PageParams = {
    // 页数
    page: number
    // 当前页中有多少个
    pageSize: number
}

// 商品定义类型
export type GoodsItemItems = {
    desc: string
    id: string
    name: string
    orderNum: number
    picture: string
    price: string
}

// 用户数据
export type baseProfile = {
    id?: string
    avatar?: string
    nickname?: string
    account?: string
}

// 地址管理数据
export type addressItem = {
    // 姓名
    receiver: string
    // 联系方式
    contact: string
    // 省份编码
    provinceCode: string
    // 城市编码
    cityCode: string
    // 区县编码
    countyCode: string
    // 详细地址
    address: string
    // 是否是默认地址
    isDefault?: number
}