// 这里存储登录类型参数
import { baseProfile } from "./global"

export type loginItem = baseProfile & {
    mobile: string
    token: string
}