// 这里存储热门推荐类型
import { GoodsItem, GoodsItemItems } from "@/types/global"

// 热门推荐类型
export type HotItem = {
    bannerPicture: string
    id: string
    subTypes: subTypesItem[]
    title: string
}
// 热门推荐子选项类型
export type subTypesItem = {
    goodsItems: GoodsItem<GoodsItemItems>,
    id: string
    title: string
}
