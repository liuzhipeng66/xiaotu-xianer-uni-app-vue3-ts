// 这里存储地址管理相关类型
import { addressItem } from "./global"

// 获取参数
export type ResultAddress = addressItem & {
    id: string
    addressTags: string
    postalCode: string
    fullLocation: string
}
// 添加地址管理(参数)
export type addressParams = addressItem
// 添加地址返回类型
export type putAddress = {
    id: string
}
