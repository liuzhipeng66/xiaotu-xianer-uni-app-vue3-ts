// 这里存储个人信息的类型
import { baseProfile } from "./global"

// 个人信息数据（token）
export type profileItem = baseProfile & {
    gender?: Gender
    // 出生日期
    birthday?: string
    // 城市
    fullLocation?: string
    // 职业
    profession?: string
    // 省份编码
    provinceCode?: string
    // 城市编码
    cityCode?: string
    // 区/县编码
    countyCode?: string
}
// 性别
export type Gender = '女' | '男'

// // 个人信息修改类型
// export type profileSubmit = {

// }