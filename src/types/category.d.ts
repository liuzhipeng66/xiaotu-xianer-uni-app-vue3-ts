// 这里存储 分类 类型
import { GoodsItemItems } from "./global"

// 分类类型
export type categoryItem = {
    id: string
    name: string
    picture: string
    imageBanners: string[],
    children: childrenItem[]
}
// 分类子类型
export type childrenItem = {
    id: string
    name: string
    picture: string
    goods: GoodsItemItems[]
}

// 商品详情
export type goods = {
    id: string
    brand: object
    category: string[]
    mainPictures: string[]
    name: string
    desc: string
    oldPrice: string
    price: string
    spuCode: string
    similarProducts: GoodsItemItems[]
    skus: skusItem[]
    specs: specsItem[]
}
// skusItem
export type skusItem = {
    id: string
    inventory: number
    oldPrice: string
    picture: string
    price: number
    skuCode: string
    specs: specsArr[]
}
export type specsArr = {
    name: string
    valueName: string
}
// specsItem
export type specsItem = {
    id: string
    name: string
    values: specsItemValues[]
}
export type specsItemValues = {
    desc: string
    name: string
    picture: string
}