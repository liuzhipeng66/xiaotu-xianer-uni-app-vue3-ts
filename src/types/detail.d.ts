// 这里存储订单详细（参数） 类型
export type detailParams = {
    goods: goodsDetail[]
    addressId: string
    deliveryTimeType: number
    buyerMessage: string
    payType: 1 | 2
    payChannel: 1 | 2
}
export type goodsDetail = {
    skuId: string
    count: number
}

// 订单详细数据类型（主页）
export type detailOrder = {
    arrivalEstimatedTime: string
    cityCode: string
    closeTime: string
    consignTime: string
    countdown: number
    countyCode: string
    createTime: string
    deliveryTimeType: number
    endTime: string
    evaluationTime: string
    id: string
    orderState: number
    payChannel: number
    payLatestTime: string
    payMoney: number
    payState: number
    payTime: string
    payType: number
    postFee: number
    provinceCode: string
    receiverAddress: string
    receiverContact: string
    receiverMobile: string
    skus: skusDdail[]
    totalMoney: number
    totalNum: number
}
export type skusDdail = {
    attrsText: string
    curPrice: number
    id: string
    image: string
    name: string
    properties: propertiesDetail[]
    quantity: number
    realPay: number
    spuId: string
    totalMoney: string
}
export type propertiesDetail = {
    propertyMainName: string
    propertyValueName: string
}