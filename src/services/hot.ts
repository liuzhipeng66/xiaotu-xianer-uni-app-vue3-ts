// 这里存储热门推荐的API
import { http } from "@/utils/http";
import type { PageParams } from "@/types/global";
import type { HotItem } from "@/types/hot"

type hotParams = PageParams & { subType?: string }
export const getHotAPI = (url: string, data: hotParams) => {
    return http<HotItem>({
        method: 'GET',
        url,
        data
    })
}