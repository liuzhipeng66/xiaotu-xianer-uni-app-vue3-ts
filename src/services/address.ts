// 这里存储地址管理API
import { http } from "@/utils/http"
import type { ResultAddress, putAddress, addressParams } from "@/types/address"

/*
获取收货地址列表
GET
/member/address
*/
export const getMemberAddressAPI = () => {
    return http<ResultAddress[]>({
        url: "/member/address",
        method: 'GET'
    })
}

/*
添加收货地址
POST
/member/address
*/
export const postMemberAddressAPI = (data: addressParams) => {
    return http<putAddress>({
        method: 'POST',
        url: '/member/address',
        data
    })
}

/*
获取收货地址详情(修改地址信息回显API)
GET
/member/address/{id}
*/
export const getMemberAddress = (id: string) => {
    return http<ResultAddress>({
        url: `/member/address/${id}`,
        method: "GET"
    })
}

/*
修改收货地址
PUT
/member/address/{id}
*/
export const putMemberAddressAPI = (id: string, data: addressParams) => {
    return http<addressParams>({
        method: 'PUT',
        url: `/member/address/${id}`,
        data
    })
}

/*
删除收货地址
DELETE
/member/address/{id}
*/
export const deleteMemberAddressAPI = (id: string) => {
    return http<putAddress>({
        method: 'DELETE',
        url: `/member/address/${id}`
    })
}