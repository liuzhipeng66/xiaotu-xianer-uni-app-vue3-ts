import type { OrderLogistics, getOrderParams, getOrderItem } from "@/types/pay"
import { http } from "@/utils/http";

/* 
模拟支付-内测版
GET
/pay/mock
*/
export const getPayMockAPI = (data: { orderId: string }) => {
    return http({
        method: 'GET',
        url: '/pay/mock',
        data
    })
}

/*
模拟发货-内测版
开发中
GET
/member/order/consignment/{id}
*/
export const getMemberOrderConsignment = (id: string) => {
    return http({
        method: 'GET',
        url: `/member/order/consignment/${id}`
    })
}

/*
确认收货
PUT
/member/order/{id}/receipt
*/
export const getMemberOrderReceiptAPI = (id: string) => {
    return http({
        method: 'PUT',
        url: `/member/order/${id}/receipt`
    })
}

/*
获取订单物流
开发中
GET
/member/order/{id}/logistics
仅在订单状态为待收货，待评价，已完成时，可获取物流信息。
@params id: 订单id
*/
export const getMemberOrderLogistics = (id: string) => {
    return http<OrderLogistics>({
        method: 'GET',
        url: `/member/order/${id}/logistics`
    })
}

/*
删除订单
开发中
DELETE
/member/order
*/
export const deleteMemberOrder = (data: {
    ids: string[]
}) => {
    return http<OrderLogistics>({
        method: 'DELETE',
        url: `/member/order`,
        data
    })
}

/*
获取订单列表
GET
/member/order
*/
export const getMemberOrderAPI = (data: getOrderParams) => {
    return http<getOrderItem>({
        method: 'GET',
        url: '/member/order',
        data
    })
}