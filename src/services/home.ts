// 这里存储首页API
import { http } from "@/utils/http";
import type { GoodsItem, PageParams, GoodsItemItems } from "@/types/global";
import type { BannerItem, CategoryItem, HotItem } from '@/types/home'

/*
首页-广告区域-小程序
GET
/home/banner
*/
export const getHomeBannerAPI = (distributionSite = 1) => {
    return http<BannerItem[]>({
        method: 'GET',
        url: '/home/banner',
        data: {
            distributionSite
        }
    })
}

/*
首页-前台分类-小程序
GET
/home/category/mutli
*/
export const getHomeCategoryMutliAPI = () => {
    return http<CategoryItem[]>({
        url: '/home/category/mutli',
        method: 'GET'
    })
}

/*
首页-热门推荐-小程序
GET
/home/hot/mutli
*/
export const getHomeHotMutliAPI = () => {
    return http<HotItem[]>({
        url: '/home/hot/mutli',
        method: 'GET'
    })
}

/*
猜你喜欢-小程序
GET
/home/goods/guessLike
*/
export const getHomeGoodsGuessLikeAPI = (pageParams: PageParams) => {
    return http<GoodsItem<GoodsItemItems>>({
        url: '/home/goods/guessLike',
        method: 'GET',
        data: pageParams
    })
}