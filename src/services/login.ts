// 这里存储登录API
import { http } from "@/utils/http";
import type { loginItem } from "@/types/login"

/*
小程序登录-内测版
POST
/login/wxMin/simple
*/
export const postLoginWxMinSimple = (phoneNumber: string) => {
    return http<loginItem>({
        url: '/login/wxMin/simple',
        method: 'POST',
        data: {
            phoneNumber
        }
    })
}