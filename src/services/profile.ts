// 这里存储个人信息API
import { http } from "@/utils/http";
import type { profileItem } from "@/types/profile"

/*
获取个人信息
GET
/member/profile
*/
export const getMemberProfileAPI = () => {
    return http<profileItem>({
        url: '/member/profile',
        method: "GET"
    })
}

/*
修改个人信息
PUT
/member/profile
*/
export const putMemberProfile = (data: profileItem) => {
    return http<profileItem>({
        method: 'PUT',
        url: '/member/profile',
        data
    })
}