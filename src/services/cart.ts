// 这里存储购物车API
import type { cartItem } from "@/types/cart"
import { http } from "@/utils/http";

/*
加入购物车
POST
/member/cart
*/
export const postMemberCartAPI = (data: {
    skuId: string, count: number
}) => {
    return http({
        url: "/member/cart",
        method: "POST",
        data
    })
}

/*
获取购物车列表
GET
/member/cart
*/
export const getMemberCartAPI = () => {
    return http<cartItem[]>({
        url: '/member/cart',
        method: 'GET'
    })
}

/*
* 删除/清空购物车单品
* DELETE
* /member/cart
* @param ids: skuId
*/
export const deleteMemberCartAPI = (data: { ids: string[] }) => {
    return http({
        url: '/member/cart',
        method: 'DELETE',
        data
    })
}

/**
修改购物车单品
PUT
/member/cart/{skuId}
*/
export const putMemberCartAPI = (skuId: string, data: { selected?: boolean, count?: number }) => {
    return http({
        url: `/member/cart/${skuId}`,
        method: 'PUT',
        data
    })
}

/*
购物车全选/取消全选
PUT
/member/cart/selected
*/
export const putMemberCartSelected = (data: { selected: boolean }) => {
    return http({
        url: `/member/cart/selected`,
        method: 'PUT',
        data
    })
}