// 这里存储分类的API
import { http } from "@/utils/http";
import type { categoryItem, goods } from "@/types/category"

/* 
分类列表-小程序
GET
/category/top
*/
export const getCategoryTop = () => {
    return http<categoryItem[]>({
        method: 'GET',
        url: '/category/top',
    })
}

/*
商品详情
GET
/goods
*/
export const getGoodAPI = (id: string) => {
    return http<goods>({
        url: '/goods',
        method: 'GET',
        data: { id }
    })
}