// 这里存储订单数据API
import type { orderItem } from "@/types/order"
import { http } from "@/utils/http";

/*
填写订单-获取预付订单
GET
/member/order/pre
*/
export const getMemberOrder = () => {
    return http<orderItem>({
        method: 'GET',
        url: '/member/order/pre'
    })
}

/*
填写订单-获取立即购买订单
GET
/member/order/pre/now
 skuId 商品skuId 必需
count 购买商品的数量 必需
addressId 可选 下单时已经选择好的地址id
*/
export const getMemberOrderPreNow = (data: {
    skuId: string,
    count: string,
    addressId?: string
}) => {
    return http<orderItem>({
        method: 'GET',
        url: '/member/order/pre/now',
        data
    })
}
