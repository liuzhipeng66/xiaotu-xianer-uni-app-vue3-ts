import type { detailParams, detailOrder } from "@/types/detail"
import type { orderItem } from "@/types/order"
import { http } from "@/utils/http"

/*
提交订单
POST
/member/order
*/
export const postMemberOrder = (data: detailParams) => {
    return http<{ id: string }>({
        url: '/member/order',
        method: 'POST',
        data
    })
}

/*
获取订单详情
GET
/member/order/{id}
*/
export const getMemberOrder = (id: string) => {
    return http<detailOrder>({
        method: 'GET',
        url: `/member/order/${id}`
    })
}

/*
填写订单-获取再次购买订单
GET
/member/order/repurchase/{id}
*/
export const getMemberOrderRepurchase = (id: string) => {
    return http<orderItem>({
        method: 'GET',
        url: `/member/order/repurchase/${id}`
    })
}