// 存储组合式API
import { ref } from "vue"

// 猜你喜欢数据 组合式封装
export const useGuessList = () => {
    // XtxGuess 实例
    const guessRef = ref()
    // 上滑加载事件回调
    const onScrolltolower = () => {
        guessRef.value.getMore()
    }

    // 暴露方法
    return {
        guessRef,
        onScrolltolower
    }
}